# HSH Requests Web Application #

HSH Requests Web Application - Java/Backend Repository

### What is this repository for? ###

* Spring Web Project
* Handles the backend: Database management, output, reports, etc.
* HSHR Version 3.0

### How do I get set up? ###

* Clone HSHR-UI repository
* Clone this repository
* (OPTIONAL) Clone HSHR-Docs repository for documentation and other application related files
* Install Node.js, JDK v16, Angular, IDE of choise (VSCode, Eclipse, etc.), & PostgreSQL.
* If you are using VSCode add the Spring Extention Pack & Java Extention Pack
* Copy your postgre server information to this project's application settings and change the first line to say Create instead of None.
* Run both this and HSHR-UI together
* After first run change this project's application settings first line back to None.
